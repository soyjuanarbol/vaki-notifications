const dotenv = require('dotenv')

// function load .env files with node
const loadEnvVars = () => {
  return dotenv.config({ path: './config.env' })
}

module.exports = loadEnvVars
