const loadEnvironments = require('./environments')
const nodemailer = require('nodemailer')
const mandrillTransport = require('nodemailer-mandrill-transport')

// Load env vars and create a new Mailer Object
// One time for a singleton effect!
loadEnvironments()
const mailerConfig = {
  auth: {
    apiKey: process.env.MANDRILL_API_KEY
  }
}

class Mailer {
  constructor () {
    this.transport = nodemailer.createTransport(mandrillTransport(mailerConfig))
  }

  /**
   * Method for sending emails using mandrill transport
   * and params like from, to, template...
   * @param {Object} emailOptions mail information
   */
  sendEmail (emailOptions) {
    let mailerOptions = {
      from: 'contacto@vaki.co',
      mandrillOptions: {
        template_content: [
          {
            name: 'main',
            content: ''
          }
        ],
        message: {
          merge: true,
          merge_language: 'mailchimp',
          from_name: 'Lola de Vaki'
        }
      }
    }

    if (process.env.NODE_ENV === 'PRODUCTION') {
      mailerOptions.mandrillOptions.message.bcc_address = 'contacto@vaki.co'
    }

    mailerOptions.to = emailOptions.to
    mailerOptions.subject = emailOptions.subject || 'Lola de Vaki'
    mailerOptions.mandrillOptions.template_name = emailOptions.mandrillOptions.template_name
    mailerOptions.mandrillOptions.message.global_merge_vars = { ...emailOptions.mandrillOptions.message.global_merge_vars } || []

    // This is a EXTREMELEY PARTICULAR CASE, IF HAS OTHER SOLUTION, PLEASE change this one
    // When it's a Vaki's update, remove bcc for avoid bcc receive 10000000 mails
    if (emailOptions.mandrillOptions.template_name === 'web-2-0-actualizacion-vaquinha-aportante') {
      const recipient = emailOptions.to
      const recipientList = [recipient, 'contacto@vaki.co']

      mailerOptions.mandrillOptions.message.bcc_address = null
      mailerOptions.to = recipientList
    }

    return this.transport.sendMail(mailerOptions, (error, info) => {
      if (error) {
        return error
      }
      return info
    })
  }
}

module.exports = Mailer
