const Mailer = require('./lib/Mailer')
const { send, json } = require('micro')
const cors = require('micro-cors')()

const mailer = new Mailer()

const handler = async (req, res) => {
  // parse POST request params then email to that received data
  const mailOptions = await json(req)
  mailer.sendEmail(mailOptions)
  send(res, 200)
}

module.exports = cors(handler)
